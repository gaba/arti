BREAKING: Channel is now explicitly wrapped in Arc<> wherever it
occurs.  Previously, it was an implicitly Arc<> type.

